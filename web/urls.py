from django.contrib import admin
from django.urls import path
from myweb.views import *
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),  
    #pengenalan
    path('', pengenal, name='pengenal'),


    #view
    path('beranda/', views.beranda),

    #projec utama
    path('home/', home, name='home'),
    
    path('tutorial/', tutorial, name='tutorial'),

    path('book/', book, name='book'),

    path('beranda/galeri/', galeri, name='galeri'),

    path('about/', about, name='about'),
]
